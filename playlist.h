#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#pragma comment(lib,"winmm.lib")
using namespace std;

#define center "\t\t"

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
////////////////////CREATING MY LIST///////////////////
class list
{
	private:
		typedef struct node
		{
			int s;
			int sPlaylist;
			int sQueue;
			int dGenre;
			string sTitle;
			string sArtist;
			string sGenre;
			string sAlbum;
			bool inPlaylist;
			bool inQueue;
			
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;

	public:
		list();
		void pushNode(string sTitle,string sArtist ,string sGenre ,string sAlbum,int dataGenre);
		void display();
		int addNodePlaylist(int addNode, int counter);
		int removePlaylist(int counter);
		void viewPlaylist();
		int enQueue(int qS ,int counter);
		int deQueue();
		void sortByAlbum();
		void printQueue(int queueCounter);
		void playQueue(int queueCounter);
		void playStack(int stackCounter);
		void RandomPlaylist(int random);
};

//INSTANTIATING POINTERS//
list::list()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}

//PUSH NODE / CREATE NODE//
void list::pushNode(string sTitle,string sArtist ,string sGenre ,string sAlbum, int dataGenre)
{
	nodePtr n = new node;
	n->next = NULL;
	if(head!=NULL)
	{
		curr = head;
		temp = head;
		while(curr->next != NULL)
		{
			curr = curr->next;
			temp = curr;
		}
		curr->next =n;
		curr = curr->next;
		curr->s = temp->s+1;
		//PUT COMPONENTS OF NODE HERE
		system("CLS");
		SetConsoleTextAttribute(hConsole, 4);  
		cout <<"Please wait.... "<< endl;
	}
	else
	{
		head = n;
		n->s =0;
	}
	top = n;
	n->sTitle = sTitle;
	n->sArtist = sArtist;
	n->sGenre = sGenre;
	n->sAlbum = sAlbum;
	n->inPlaylist = false;
	n->inQueue = false;
	n->sPlaylist =0;
	n->sQueue =0;
	n->sGenre = dataGenre;
}

//DISPLAY STACK//
void list::display()
{
	cout << "COUNTER: "<<top->s<<endl;
	cout <<center<< "                             My Playlist                                     " << endl;
	cout <<center<< "								List										  " << endl;
	for(int i = 1;i<=top->s;i++)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->s!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->s==0)
		{
		}
		else
		{
			cout << curr->s << " " << curr->sTitle << "||" << curr->sAlbum << "||" <<curr->sGenre << endl;
		}
	} 

}

//VIEWING PLAYLIST//
void list::viewPlaylist()
{
	cout <<center<< "                             My Playlist                                     " << endl;
	cout <<center<< "								List										  " << endl;
	for(int i = top->s;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->sPlaylist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->s==0||curr->sPlaylist==0||curr->inPlaylist==false)
		{
		}
		else
		{
			cout << curr->sPlaylist << " " << curr->sTitle << "||" << curr->sAlbum << "||" <<curr->sGenre << endl;
		}
	}

}

//VIEWING QUEUE//
void list::printQueue(int queueCounter)
{
	cout <<center<< "                             My Playlist                                     " << endl;
	cout <<center<< "								List										  " << endl;
	if(queueCounter==1)
	{
		cout << "EMPTY" <<endl;
	}
	else
	{
		for(int i=0;i<=top->s;i++)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->sQueue!=i)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->s==0||curr->sQueue==0||curr->inQueue==false)
			{
			}
			else
			{
				cout << curr->sQueue << " " << curr->sTitle << "||" << curr->sAlbum << "||" <<curr->sGenre << endl;
			}
		}	
	}

}

//ADDING TO PLAYLIST//
int list::addNodePlaylist(int addNode, int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> s != addNode)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->s ==0)
	{
		cout << addNode << " No song available \n";
		return 0;
	}
	else if(curr->inPlaylist==true)
	{
		return 0;
	}
	else
	{	
		curr->sPlaylist = counter;
		curr->inPlaylist =true;
		return 1; 
	}
}

//REMOVING FROM PLAYLIST//
int list::removePlaylist(int counter)
{
	int delData = counter -1;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> sPlaylist != delData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->s ==0)
	{
		return 0;
	}
	else if(curr->inPlaylist==false)
	{
		return 0;
	}
	else
	{	
		cout << "REMOVED"<< endl;
		curr->sPlaylist = 0;
		curr->inPlaylist =false;
		return 1; 
	}
}

//ENQUEUE / ADD TO QUEUE
int list::enQueue(int qS,int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> s != qS)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->s ==0)
	{
		cout << qS << " No song availble \n";
		return 0;
	}
	else if(curr->inQueue==true)
	{
		return 0;
	}
	else
	{	
		curr->sQueue = counter;
		curr->inQueue =true;
		return 1; 
	}
}

//DEQUEUE / REMOVE FROM QUEUE
int list::deQueue()
{
	curr=head;
	while(curr->next!=NULL)
	{
		if(curr->sQueue!=0)
		{
			curr->sQueue=curr->sQueue-1;
			if(curr->sQueue==0)
			{
				curr->inQueue=false;			
			}
		}
		curr=curr->next;
	}
	return 1;
}

//PLAY PLAYLIST
void list::playStack(int stackCounter)
{
	int i=1;
	while(i<=stackCounter)
	{
		if(stackCounter==1)
		{
			break;
		}
		system("CLS");
		cout <<center<< "=======================================PLAYLIST=======================================" << endl;
		for(int x = top->s;x>=0;x--)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->sPlaylist!=x)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->s==0||curr->sPlaylist==0||curr->inPlaylist==false)
			{
			}
			else
			{
				cout << curr->sPlaylist << " " << curr->sTitle << "||" << curr->sAlbum << "||" <<curr->sGenre << endl;
			}
		}
		curr=head;
		while(i-1!=curr->sPlaylist)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		
		cout <<center<< "|      CHOOSE:                                                                         |"<< endl
			 <<center<< "|      1:NEXT                                                                          |"<< endl
			 <<center<< "|      2:BACK                                                                          |"<< endl																  
	
			 <<"NOW PLAYING: "<< curr->sTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION 	
		cout << "INPUT: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}
}

//PLAY QUEUE
void list::playQueue(int queueCounter)
{
	int i=1;
	while(i<=queueCounter)
	{
		if(queueCounter==1)
		{
			break;
		}
		system("CLS");
		cout <<center<< "========================================QUEUE=========================================" << endl;
		if(queueCounter==1)
		{
			cout << "EMPTY" <<endl;
		}
		else
		{
			for(int x=0;x<=top->s;x++)
			{
				curr=head;
				temp=head;
				while(curr!=NULL&&curr->sQueue!=x)
				{
					curr = curr->next;
				}
				if(curr==NULL||curr->s==0||curr->sQueue==0||curr->inQueue==false)
				{
				}
				else
				{
					cout << curr->sQueue << " " << curr->sTitle << "||" << curr->sAlbum << "||" <<curr->sGenre << endl;
				}
			}	
		}
		cout <<center<< "======================================================================================" << endl;
		
		curr=head;
		while(i-1!=curr->sQueue)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout <<center<< "========================================QUEUE==========================================="<<endl
			 <<center<< "|      CHOOSE:                                                                         |"<< endl
			 <<center<< "|      1:NEXT                                                                          |"<< endl
			 <<center<< "|      2:BACK                                                                          |"<< endl																  
			 <<center<< "========================================================================================"<< endl
			 <<"NOW PLAYING: "<< curr->sTitle <<endl;//PLAYING SONG HERE
		//PLAY MUSIC FUNCTION FUCKING SHIT	
		cout << "INPUT: ";
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}	
}
// SELECTION SORTING
void list::sortByAlbum()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int genre = 1;
	for(genre;genre<=10;genre++)
	{
		curr=head;
		temp=head;
		while(curr->next!=NULL)//Scan the whole linked list
		{
			if(curr->dGenre == genre)
			{
				SetConsoleTextAttribute(hConsole, genre);  
				cout << curr->sGenre <<"||" << curr->sTitle << "||" << curr->sAlbum << "||" <<endl;
			}
			curr=curr->next;
		}
	}
}

//RANDOM PLAYLIST
void list::RandomPlaylist(int random)
{
	int i=1;
	while(i<=13)
	{
		int x=1;
		while(curr->next!=NULL&&x<=10)
		{
			if(curr->dGenre==random)
			{
				cout << curr->sGenre <<"||" << curr->sTitle << "||" << curr->sAlbum << "||" <<endl;
				x=x+1;
			}
			curr=curr->next;
		}
	}
}

